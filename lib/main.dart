import 'package:flutter/material.dart';
import 'widgets/home.dart';
import 'models/event_data.dart';

const appTitle = 'Cosplay2 Validator';

void main() => runApp(Cosplay2Validator());

class Cosplay2Validator extends StatelessWidget {
  static const _brightness = Brightness.dark;
  static final ColorScheme? _colorScheme = _brightness == Brightness.light
      ? const ColorScheme.light()
      : _brightness == Brightness.dark
          ? const ColorScheme.dark()
          : null;

  final theme = ThemeData(
    brightness: _brightness,
    colorScheme: _colorScheme,
  );

  final mockEventData = EventData("Yuki no Odori", {
    Ticket.newSold(TicketInfo.autoKind("7423G1C468B686C"),
        TicketOwner(PersonName("Himura"))),
    Ticket.newReturned(TicketInfo.autoKind("8923K1C468B683D"),
        TicketOwner(PersonName("Рандомный чел"))),
    Ticket.newSold(
        TicketInfo.autoKind("8963LR6", type: "VIP", number: "1-300"),
        TicketOwner(PersonName("Kazuto", first: "Himura", middle: "Максимович"),
            city: "Туда", phoneNumber: "+7 999 123 45 97"),
        sold: DateTime.now()),
  });

  Cosplay2Validator({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: appTitle,
        theme: theme,
        home: Home(mockEventData), // TODO: https://pub.dev/packages/local_auth
      );
}
