import 'package:flutter/material.dart';
import '../models/event_data.dart';
import 'ticket_list.dart';

class Home extends StatefulWidget {
  final EventData event;
  const Home(this.event, {super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late final EventData event;
  late final _MenuButton _menuButton;
  late TicketList _ticketList;
  final _searchFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    event = widget.event;
    _ticketList = TicketList(event.tickets);
    _menuButton = _MenuButton(_onMenuItemSelected);
  }

  @override
  Widget build(BuildContext context) {
    var bottomSystemUIHeigth = MediaQuery.of(context).viewInsets.bottom +
        MediaQuery.of(context).padding.bottom;

    return Scaffold(
        resizeToAvoidBottomInset:
            false, // Reposition manually for keyboard not to overlap BottomBar
        appBar: AppBar(
          title: Text(event.name),
          actions: [
            IconButton(
              icon: const Icon(Icons.sync),
              onPressed: _sync,
            ),
            _menuButton,
          ],
        ),
        body: Padding(
          padding: EdgeInsets.only(bottom: bottomSystemUIHeigth),
          child: ListView(
            padding: const EdgeInsets.all(15),
            reverse: true,
            children: _ticketList.getTiles(context, _updateTicketSelection),
          ),
        ),
        bottomNavigationBar: Transform.translate(
          offset: Offset(0.0, -1 * bottomSystemUIHeigth),
          child: _BottomBar(
            _filterTickets,
            _switchQrScanner,
            _clearSearchField,
            _searchFieldController
          ),
        ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: bottomSystemUIHeigth),
          child: _ActionButton(_markSelectedTicketUsed),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat);
  }

  void _updateTicketSelection(Ticket newSelectedTicket) {
    setState(() {
      _ticketList.selectedTicket = newSelectedTicket;
    });
  }

  void _markSelectedTicketUsed() {
    if (_ticketList.selectedTicket == null) {
      return;
    }
    setState(() {
      _ticketList.selectedTicket!.status = TicketStatus.used;
      _ticketList.selectedTicket!.used = DateTime.now();
      _ticketList.tickets = {_ticketList.selectedTicket!};
      _searchFieldController.clear();
    });
  }

  void _filterTickets(query) {
    setState(() {
      _ticketList = TicketList.fromFilter(event.tickets, query);
    });
  }

  void _switchQrScanner() {
    // TODO
    // https://pub.dev/packages/qr_code_scanner
  }

  void _clearSearchField() {
    _searchFieldController.clear();
    _filterTickets(_searchFieldController.text);
  }

  void _onMenuItemSelected(_MenuItem itemType) {
    switch (itemType) {
      case _MenuItem.allTickets:
        setState(() {
          _ticketList = TicketList(event.tickets);
        });
        break;
    }
  }

  void _sync() {
    // TODO
    // https://pub.dev/packages/google_sign_in
    // https://pub.dev/packages/extension_google_sign_in_as_googleapis_auth
    // https://pub.dev/packages/googleapis
  }

  @override
  void dispose() {
    _searchFieldController.dispose();
    super.dispose();
  }
}

class _MenuButton extends StatelessWidget {
  final Function(_MenuItem) onMenuItemSelected;
  const _MenuButton(this.onMenuItemSelected);

  @override
  Widget build(BuildContext context) => PopupMenuButton<_MenuItem>(
        onSelected: onMenuItemSelected,
        itemBuilder: (context) => [
          const PopupMenuItem(
            value: _MenuItem.allTickets,
            child: Text("Все билеты"),
          )
        ],
      );
}

enum _MenuItem { allTickets }

class _BottomBar extends StatelessWidget {
  final Function(String) onSearchInput;
  final Function() onQrPressed;
  final Function() onBackspacePressed;
  final TextEditingController searchFieldController;

  const _BottomBar(
    this.onSearchInput,
    this.onQrPressed,
    this.onBackspacePressed,
    this.searchFieldController
  );

  @override
  Widget build(BuildContext context) => BottomAppBar(
        child: Row(
          children: [
            IconButton(
              icon: const Icon(Icons.qr_code),
              onPressed: onQrPressed,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: TextField(
                  autofocus: true,
                  autocorrect: false,
                  decoration: const InputDecoration(hintText: 'Искать билет'),
                  style: const TextStyle(fontSize: 24),
                  onChanged: onSearchInput,
                  controller: searchFieldController,
                ),
              ),
            ),
            IconButton(
              icon: const Icon(Icons.backspace),
              onPressed: onBackspacePressed,
            ),
          ],
        ),
      );
}

class _ActionButton extends StatelessWidget {
  final Function() onPressed;
  const _ActionButton(this.onPressed);

  @override
  Widget build(BuildContext context) => FloatingActionButton(
        onPressed: onPressed,
        child: const Icon(Icons.exit_to_app),
      );
}
