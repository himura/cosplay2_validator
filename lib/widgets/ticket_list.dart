import '../models/event_data.dart';
import 'package:flutter/material.dart';

class TicketList {
  Set<Ticket> tickets;
  Ticket? selectedTicket;

  TicketList(this.tickets, {this.selectedTicket});

  static TicketList get empty => TicketList(<Ticket>{});

  static TicketList fromFilter(Set<Ticket> allTickets, String query) {
    if (query.isEmpty) {
      return TicketList.empty;
    }
    var firstChar = query.codeUnitAt(0);
    // [А-Яа-я]
    if (firstChar > 1040 && firstChar < 1103) {
      return _filterByLastName(allTickets, query);
    } else {
      var candidates = _filterByCode(allTickets, query);
      if (candidates.selectedTicket == null) {
        candidates = _filterByLastName(allTickets, query);
      }
      return candidates;
    }
  }

  List<Widget> getTiles(BuildContext context, void Function(Ticket) onTileTap) {
    return tickets.map((ticket) {
      final backgroundColor = ticket.status == TicketStatus.ok
          ? Theme.of(context).colorScheme.primary
          : Theme.of(context).colorScheme.error;
      final textStyle = ticket == selectedTicket
          ? TextStyle(
              color: ticket.status == TicketStatus.ok
                  ? Theme.of(context).colorScheme.onPrimary
                  : Theme.of(context).colorScheme.onError)
          : null;

      // TODO: full information on selected ticket
      return ListTile(
        leading: Icon(ticket.info.kind == TicketKind.deadTree
            ? Icons.confirmation_number
            : Icons.smartphone),
        title: Text(ticket.owner.name.toString(), style: textStyle),
        subtitle: Text(ticket.info.code, style: textStyle),
        selected: ticket == selectedTicket,
        selectedTileColor: backgroundColor,
        onTap: () {
          onTileTap(ticket);
        },
      );
    }).toList();
  }

  static TicketList _filterByLastName(Set<Ticket> tickets, String query) {
    var allCandidates = tickets.where((ticket) =>
        ticket.owner.name.last.toLowerCase().contains(query.toLowerCase()));
    if (allCandidates.isEmpty) {
      return TicketList.empty;
    }
    var bestCandidates = allCandidates.where((ticket) =>
        ticket.owner.name.last.toLowerCase().startsWith(query.toLowerCase()));
    var candidates = bestCandidates.toSet();
    candidates.addAll(allCandidates.toSet().difference(candidates));
    return TicketList(candidates, selectedTicket: candidates.first);
  }

  static TicketList _filterByCode(Set<Ticket> tickets, String query) {
    var allCandidates = tickets.where((ticket) =>
        ticket.info.code.toLowerCase().contains(query.toLowerCase()));
    if (allCandidates.isEmpty) {
      return TicketList.empty;
    }
    var bestCandidates = allCandidates.where((ticket) =>
        ticket.info.code.toLowerCase().startsWith(query.toLowerCase()));
    var candidates = bestCandidates.toSet();
    candidates.addAll(allCandidates.toSet().difference(candidates));
    return TicketList(candidates, selectedTicket: candidates.first);
  }
}
