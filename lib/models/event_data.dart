class EventData {
  final String name;
  final Set<Ticket> tickets;

  EventData(this.name, this.tickets);
}

class Ticket {
  final TicketInfo info;
  final TicketOwner owner;
  TicketStatus status;
  final DateTime? sold;
  DateTime? used;

  Ticket(this.info, this.owner, this.status, {this.sold});

  static Ticket newSold(info, owner, {sold}) =>
      Ticket(info, owner, TicketStatus.ok, sold: sold);

  static Ticket newReturned(info, owner, {sold}) =>
      Ticket(info, owner, TicketStatus.returned, sold: sold);
}

enum TicketStatus { ok, used, returned }

class TicketInfo {
  final String code;
  final TicketKind? kind;
  final String? type, number;

  TicketInfo(this.code, {this.kind, this.type, this.number});

  static const cosplay2TicketCodeLength = 15;

  static TicketInfo autoKind(code, {type, number}) {
    TicketKind kind = code.length == cosplay2TicketCodeLength
        ? TicketKind.online
        : TicketKind.deadTree;
    return TicketInfo(code, kind: kind, type: type, number: number);
  }
}

enum TicketKind { online, deadTree }

class TicketOwner {
  final PersonName name;
  final String? city, phoneNumber;

  TicketOwner(this.name, {this.city, this.phoneNumber});
}

class PersonName {
  final String? first;
  final String? middle;
  final String last;

  PersonName(this.last, {this.first, this.middle});

  @override
  String toString() {
    var fullName = last;
    if (first != null) {
      fullName += " ${first!}";
    }
    if (middle != null) {
      fullName += " ${middle!}";
    }
    return fullName;
  }
}
